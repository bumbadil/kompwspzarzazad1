#ifndef ZADANIE_H
#define ZADANIE_H

class Zadanie{

    int czas_trwania_zadania;
    int numer_zadania;
    int ilosc_nastepnych_zadani;

public:
    //Zadanie();
    void dodaj_czas_zadania(int wprowadzony_czas_zadania);
    void stworz_zadania(int numer_dodanego_zadania, int czas_zadania);
};
#endif // !ZADANIE_H
