#ifndef GRAF_H
#define GRAF_H

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include "zadanie.h"
using namespace std;

class Graf{

    int iloscWierzcholkow, iloscKrawedzi;
    vector <int> vectorWag;
    vector <vector <int>> vectorSasiedztwa;
public:
    //Graf(int podanaIloscWierzcholkow, int podanaIloscKrawedzi);
    Graf();
    ~Graf();
    void dodajDoVectoraWag(int wartoscDoDodania);
    void wczytajRozmiar(int wierz, int kraw);
    //void stworz_zadania(int numer, Zadanie nowe_zadanie);
    void wyznaczenie_stopni_grafu(vector<int> &stopnie_grafu, vector<vector<int>> vector_do_posortowania);
    void przywrocenie_topologi_grafu();
    int zwroc_indeks_najwiekszego(vector<int> stopnie_grafu);
    void stworzKrawedzie();
    void wczytajKrawedzie(int start, int stop);
    void pokazGraf();

};

#endif // !GRAF_H
